package im.jammer

class PostProcessorService {

    static transactional = true

    def process(String message, String jid) {
	    Jabber jab = Jabber.findByJabber(jid)
	    User user = User.read(jab.user.id);
	    
	    Post post = new Post(user : user, content : message);
	    if(post.save(flush : true) && !post.hasErrors()){
		    JabberMessageSender(jid, "Post @" + post.id + " successfully created")
		    notifySubscribers(post)
		    //post successfully added. send notifications
	    } else { 
		    JabberMessageSender(jid, "Error: unable to create post")
		    //error. notify sender
	    }

	    
    }
    def notifySubscribers(Post post){
	    //find subscribers to user's blog
	    //notify each about new post
    }
}
