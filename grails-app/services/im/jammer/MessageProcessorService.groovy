package im.jammer

class MessageProcessorService {
	//	MessageSenderService messageSenderService
    static transactional = true
    void newMessage(String message, String jid) {
	    println "new message!";
	    def commandCheck = message =~ /@[^\s]+/
	    String command
	    //detect message type
	    if(commandCheck.size() == 1){ 
		    command = commandCheck[0].replaceFirst("@", "")
		    if(command =~ /^[0-9]+$/){ 
			    newReply(message.replaceFirst("@" + command, ""), command, jid)
		    } else {
			    newCommand(command, command.replaceFirst("@" + command, "").split(), jid)
		    }
	    } else { 
		    newPost(message, jid)
		    //just a post
	    }
    }
    void newPost(String message, String jid){ 
	    println "new post!";
	    //check rights
	    Jabber jab = Jabber.findByJabber(jid)
	    if(!jab){
		    messageSenderService.sendMessage("You ain't allowed to post, please register first", jid)
		    //you aint allowed to post, please register first
	    }
	    User user = User.read(jab.user.id)
	    Post post = new Post(user : user, content : message)
	    if(post.save(flush : true) && !post.hasErrors()){ 
		    //post successfully added. send notifications
	    } else { 
		    //error. notify sender
	    }
    }
	void newReply(String message, Long replyTo, String jid){ 
		Jabber jab = Jabber.findByJabber(jid)
		if(!jab){
			messageSenderService.sendMessage("You ain't allowed to post, please register first", jid)
		    //you aint allowed to post, please register first
	    }
		User user = User.read(jab.user.id)
	    Reply reply = new Reply(user : user, contect : message)
	    if(reply.save(flush : true) && !reply.hasErrors()){ 
		    //post successfully added. send notifications
	    } else {
		    //error. notify sender
	    }
	}
	void newCommand(String command, String[] args){
		switch(command){ 
		case 'help':
			break;
		case 'reg':
			break;
		case 'del':
			break;
		case 'follow':
			break;
		case 'unfollow':
			break;
		}
	}
}
