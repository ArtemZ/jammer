package im.jammer

class Post {
	String content;
	String type;
	static belongsTo = [user : User]
	static hasMany = [ replies : Reply ]
    static constraints = {
	    type(inList : ['text', 'image', 'audio', 'video'])
    }
}
