package im.jammer

class Reply {
	Reply inReply
	String content
	static belongsTo = [ post : Post, user : User ]
    static constraints = {
	    inReply(nullable : true)
    }
}
