package im.jammer

class Jabber {
	String jabber
	static belongsTo = [user : User]
    static constraints = {
	    jabber(email : true)
    }
}
