package im.jammer
import im.jammer.JabberTools;
import grails.test.*


class JabberToolsTests extends GrailsUnitTestCase {
	
	//	def messageProcessorService
    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }
    void testIsPost() {
	    def post = """sdf@sdfsdfsdfsd
sdsamdsa @12312 #nfsd jknfsdlfhbdkghvdkfhbdsjhf
sadsadsadadasdad""";
	    assert JabberTools.isPost(post) == true
    }
	void testIsReply(){ 
		def reply = "  @324239482 sdfjdsbdflfdb ldksfds @sdfnsd @23424234"
		assertTrue JabberTools.isReply(reply)
	}
	void testIsCommand(){ 
		def command = " @dsfsdfsd dsfsdf sfff ddsdf 324312 @1231"
		assertTrue JabberTools.isCommand(command)
	}
	void testJidCleaner(){
		def jid = " dsad@sdssadasd.com/sdsadsadasd3423 ";
		assertEquals(JabberTools.cleanJid(jid), "dsad@sdssadasd.com")
	}
	void testTagsParser(){
		def message = "#SDFNS #sdsada #9jdas #тест #sdhbfs fsdkfnsdf#sjdfh               #ds"
		assertTrue JabberTools.parseTags(message).contains("SDFNS")
	}
}
