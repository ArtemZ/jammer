package im.jammer;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.List;
import java.util.ArrayList;
class JabberTools {
	public static boolean isPost(String message){ 
		Pattern p = Pattern.compile("^@[^\\s]+");
		Matcher m = p.matcher(message.trim());
		if(m.groupCount() > 0){ 
		    return false;
	    } else { 
		    return true;
	    }
    }
	public static boolean isReply(String message){ 
		Pattern p = Pattern.compile("^@[0-9]+");
		Matcher m = p.matcher(message.trim());
		if(m.find()){ 
			return true;
	    } else { 
		    return false;
	    }
    }
	public static boolean isCommand(String message){ 
		Pattern p = Pattern.compile("^@[a-zA-Z]+");
		Matcher m = p.matcher(message.trim());
		if(m.find()){ 
			    return true;
	    } else { 
		    return false;
	    }
	}
	public static String cleanJid(String jid){
		Pattern p = Pattern.compile("^[^/]+");
		Matcher m = p.matcher(jid.trim());
		m.find();
		return m.group();
	}
	public static List<String> parseTags(String message){
		List<String> tags = new ArrayList<String>();
		Pattern p = Pattern.compile("(\\s|^)#[^\\s]+");
		Matcher m = p.matcher(message);
		while(m.find()){
			tags.add(m.group().trim());
		}
		return tags;
	}
}
