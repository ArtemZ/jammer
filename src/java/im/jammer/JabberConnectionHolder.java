package im.jammer;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.MessageListener;

class JabberConnectionHolder {
	private static XMPPConnection conn = null;
	public static MessageListener ml = null;
	public static XMPPConnection getConnection(){
		return conn;
	}
	public static void connect(String username, String password, String server, Integer port) throws Exception{
	    ConnectionConfiguration config = new ConnectionConfiguration(server, port);
	    config.setCompressionEnabled(true);
	    config.setSASLAuthenticationEnabled(true);
	    conn = new XMPPConnection(config);
	    // try {					
		    conn.connect();
	    // } catch(XMPPException e){
		//     throw new Exception("Connection error: " + e.getMessage());
	    // }
	    conn.login(username, password, "Grails");
    }
	public static void disconnect() throws Exception { 
		// try {
			conn.disconnect();
	    // } catch(XMPPException e){
		//     throw new Exception("Connection error: " + e.getMessage());
	    // }
	}
}
