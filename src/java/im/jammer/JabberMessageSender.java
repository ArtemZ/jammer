package im.jammer;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.XMPPConnection;

class JabberMessageSender {
	public static void sendMessage(String jid, String message) throws Exception{
		XMPPConnection conn = JabberConnectionHolder.getConnection();
		if(conn == null) throw new Exception("Not connected. Unable to send message");
		Message msg = new Message(jid);
		msg.setBody(message);
		conn.sendPacket((Packet)msg);
	}
}
