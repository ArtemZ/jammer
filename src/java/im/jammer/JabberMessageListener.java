package im.jammer;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Message;

import java.lang.reflect.Method;

import org.codehaus.groovy.grails.web.servlet.GrailsApplicationAttributes;
import org.codehaus.groovy.grails.web.context.ServletContextHolder;
import org.springframework.context.ApplicationContext;

//import org.codehaus.groovy.grails.web.context.ServletContextHolder;
//import org.codehaus.groovy.grails.web.servlet.GrailsApplicationAttributes;

//import java.lang.reflect.*;
class JabberMessageListener implements PacketListener {
	//	MessageProcessorService messageProcessorService;
	MessageProcessorService messageProcessor;

	public void setMessageProcessor(MessageProcessorService msgp) { 
		
		this.messageProcessor = msgp; 
	}
	//	@Override
	public void processPacket(Packet packet) {
		Message message = (Message) packet;
		String from = message.getFrom();
		String body = message.getBody();
		// Class personClass = messageProcessor.class;
        
        // //Get the methods
        // Method[] methods = personClass.getDeclaredMethods();
        
        // //Loop through the methods and print out their names
        // for (Method method : methods) {
        //     System.out.println(method.getName());
        // }
		if(JabberTools.isCommand()){
			
		} else if(JabberTools.isReply()){
			
		} else {
		}
		MessageProcessorService mps = (MessageProcessorService) SpringUtils.getSpringBean("messageProcessorService");
		mps.newMessage(body, from);
	}
	
}
class SpringUtils {

    static Object getSpringBean(String name) {
	    return getApplicationContext().getBean(name);
    }

    static ApplicationContext getApplicationContext() {
	    return (ApplicationContext)ServletContextHolder.getServletContext().getAttribute(GrailsApplicationAttributes.APPLICATION_CONTEXT);
    }    
}
